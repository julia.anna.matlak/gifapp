import React from "react";
import {AppBar} from "@mui/material";
import {Typography} from '@mui/material';
import {Toolbar} from "@mui/material";


const Header: React.FC = () => {
    return(
      <React.Fragment>
            <AppBar position="fixed" >
              <Typography variant="h3" align="center" sx={{fontSize: 50}}>GIPHY</Typography>
            </AppBar>
          <Toolbar />
      </React.Fragment>
    );
}
export default Header;