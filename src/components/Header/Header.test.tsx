import {render, screen} from '@testing-library/react';
import Header from './Header';

describe("Header components", ()=>{
    
    test("Checking if header display", ()=>{
        render(<Header/>);
        const element = screen.getByText("GIPHY");
        expect(element).toBeInTheDocument();
    })
});