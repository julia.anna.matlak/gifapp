import React, {useState, useRef} from "react";
import {SubmitHandler, useForm} from 'react-hook-form';
import {Button} from "@mui/material";
import {Grid} from "@mui/material";
import {TextField} from "@mui/material";

export interface State {
    searchdata: string;
}
const Form: React.FC<{setSearchData: (searchdata: string) => void}> = (props) => {
    const[inputState, setInputState]=useState(false);
    const{register, handleSubmit, formState: {errors}} = useForm();

    const inputRef = useRef();
    const inputChangeHandler = (searchdata: any) => {
        if (inputRef.current !== ""){
          setInputState(true);
        }
        if(inputRef.current === ""){
          setInputState(false);
        }
    };

    const formHandler = (e: any) => {
        props.setSearchData(e.searchdata);
        console.log('form data is', e);
    };
    
    return(
        <form onSubmit={handleSubmit(formHandler)} justify-content="center" >
        <Grid container spacing={1} item xs={12} alignItems="center" direction="column">
            <Grid item xs={12} spacing={0}>
                <TextField
                    required
                    id="gifinput"
                    type="text"
                    {...register("searchdata",{required: "Required"})}
                    inputRef={inputRef}
                    onChange={inputChangeHandler}
                    variant="outlined"
                    label="Jakiego gifa szukasz?"
                    helperText={
                        errors.searchdata?.type=== "required" ? "Submit is required": ""
                      }
                />
            </Grid>
            <Grid item alignItems="center" xs={12} >
                <Button
                    type="submit" 
                    variant= "contained"
                    color='secondary'>
                    Subbmit
                </Button>
            </Grid>  
        </Grid>  
        </form>
    );
}
export default Form;