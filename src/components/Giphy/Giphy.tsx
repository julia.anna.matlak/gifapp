import {useState,useEffect, useRef} from "react";
import axios from "axios";
import {Grid} from "@mui/material";
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';




export interface clickHandler{
    displaygif: string;
}
const Giphy: React.FC<{searchdata: string}> = (props) => {
    const [datagif, setDatagif] = useState<any[]>([]);

    useEffect(() => {
        const fetchData = async() => {
        const result = await axios("https://api.giphy.com/v1/gifs/search",
            {
            params: {
                api_key: "lfhJPgWE7idIBVoJhyGchQoQAznh8fZs",
                q: props.searchdata
                }
            });

        console.log(result);
        setDatagif(result.data.data);
        };
        fetchData();
    }, [props.searchdata]);

        
          function srcset(image: string, size: number, rows = 1, cols = 1) {
            return {
              src: `${image}?w=${size*cols}&h=${size * rows}&fit=crop&auto=format`,
              srcSet: `${image}?w=${size * cols}&h=${
              size * rows
              }&fit=crop&auto=format&dpr=2 2x`,
            };
          }
        
        const [clicked, setClicked] = useState(false);
        const [elementClicked, setElementClicked] = useState("");
        
        const inputRef = useRef();
        const imageChangeHandler = (displaygif: any) => {
            if (inputRef.current !== ""){
                setClicked(true);
            }
            if(inputRef.current === ""){
                setClicked(false);
            }
        };
        const renderGif = () => {
            return(
            <Grid   container
                    direction="column"
                    justifyContent="center"
                    alignItems="center">
                <Grid container spacing={0} justifyContent="center" item xs={12} alignItems="center">
                    {elementClicked && (
                    <img src={elementClicked} alt="Selected" className="selected" />)
                    }   
                </Grid>
                <Grid container spacing={1} item xs={12} alignItems="center">
                <ImageList
                    sx={{ width: '100%', height: '100%'}}
                    variant="quilted"
                    cols={4}
                    rowHeight={121}
                    gap={10}
                    >
                {datagif?.map((el,index) => (
                   <ImageListItem 
                        key={el.id} 
                        cols={el.cols || 1} 
                        rows={el.rows || 1}
                        className ="gif"
                        color="secondary"
                        style={{cursor:'pointer', color:'secondary'}}>
                        <img 
                            //src={el.images.fixed_height.url} 
                            {...srcset(el.images.fixed_height.url, 300, el.rows, el.cols)}
                            alt={el.title}
                            key={index}
                            style={{border: elementClicked === el.images.fixed_height.url ? "2px solid #d32f2f": ""}}
                            onClick={()=>setElementClicked(el.images.fixed_height.url)}
                            onChange={imageChangeHandler}
                            loading="lazy"
                            
                        />
                    </ImageListItem>
                    
                ))}
                </ImageList>
                </Grid>
            </Grid>
            );
        };

        return (
            <Grid  item xs={15} alignItems="center" className="gifbox">
                 {renderGif()}
                
            </Grid>
        )
    }
export default Giphy;