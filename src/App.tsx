import {useState, useEffect} from "react";
import Form from './components/Form/Form';
import Header from './components/Header/Header';
import Grid from '@mui/material/Grid';
import Giphy from './components/Giphy/Giphy';
import { ThemeProvider, createTheme, ThemeProvider as MuiThemeProvider, } from '@mui/material/styles';

function App() {
  
  const [searchdata,setSearchData]=useState<string>(""); //do Form
  
  
  //STYLOWANIE
  const theme = createTheme({
    palette: {
      primary: {
        main: '#212121',
      },
      secondary: {
        main: '#d32f2f',
      },
      background: {
        default: '#212121',
      },
    },
  });
 
  return (
    <ThemeProvider theme={theme}>
      <Grid container spacing={1} item xs={12} alignItems="center" direction="column">
        <Grid item xs={12}>
              <Header/>
        </Grid>
        <Grid item xs={12} alignItems="center">
          <Grid container spacing={0}>
            <Grid container
              direction="row"
              justifyContent="center"
              alignItems="baseline">
              <Form setSearchData={setSearchData}/>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} alignItems="center">
            <Giphy searchdata={searchdata} />
        </Grid>
      </Grid>
 
    </ThemeProvider>
    
    
  );
}

export default App;
